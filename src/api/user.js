import axios from "./axios.js"

//用户登录
export const login = (url,data)=>{return axios.post(url,data)}
//用户注册
export const register =(url,data)=>{return axios.post(url,data)}
//退出登录
export const logout = (url)=>{return axios.get(url)}
//推荐排行榜
export const rankList =(url)=>{return axios.get(url)}
// export const logout = (url,data)=>{return axios.get(url,{params:{...data}})}
//首页热映电影
export const userFile = (url)=>{return axios.post(url)}
//单独电影详情
export const singleFile =(url,data)=>{return axios.post(url,data)}
//获取某一电影评论
export const singleComment =(url,data)=>{return axios.post(url,data)}
//添加评论
export const addComment =(url,data)=>{return axios.post(url,data)}
//电影分类查询
export const sortFile =(url,data)=>{return axios.post(url,data)}
//添加评分
export const addScore =(url,data)=>{return axios.post(url,data)}
//搜索
export const searchFile =(url,data)=>{return axios.post(url,data)}
//用户评分信息
export const userScore = (url)=>{return axios.post(url)}
//用户评论信息
export const userComment = (url)=>{return axios.post(url)}