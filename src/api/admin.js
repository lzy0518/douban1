import axios from "./axios.js"
import qs from "qs"
//var request = window.axios;
// 管理员登录
export const adminlogin = (url,data)=>{return axios.post(url,data)}
// 管理员添加
export const addAdmin = (url,data)=>{return axios.post(url,data)}
// 电影添加
export const addFile = (url,data)=>{return axios.post(url,data)}
// 电影删除
export const delFile = (url,data)=>{return axios.post(url + `?${qs.stringify(data)}`)}
// 管理界面电影显示
export const checkFile = (url)=>{return axios.post(url)}
// export const checkFile = (data)=>{return request({
//     url:'',
//     method: 'post',
//     data,
// })}