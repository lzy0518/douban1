import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../components/Login.vue'
import Index from '../components/Index.vue'
import Adminlogin from '../components/Adminlogin.vue'
import Admin from '../components/Admin.vue'

Vue.use(VueRouter)

export default new VueRouter({
  routes: [
    // 重定向
    { path: '/', redirect: '/login' },
    { path: '/login', component: Login },
    { path: '/Index', component: Index },
    { path: '/Adminlogin', component: Adminlogin},
    { path: '/Admin',component: Admin},
    
  ]
})
